module lockbox.dev/cmd/migrations-runner

require (
	github.com/lib/pq v1.0.0
	github.com/rubenv/sql-migrate v0.0.0-20180704111356-3f452fc0ebeb
	lockbox.dev/accounts v0.1.0
	lockbox.dev/grants v0.1.0
	lockbox.dev/scopes v0.1.0
	lockbox.dev/tokens v0.1.0
	yall.in v0.0.1
)
